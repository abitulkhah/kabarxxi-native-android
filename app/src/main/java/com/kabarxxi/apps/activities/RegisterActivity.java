package com.kabarxxi.apps.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.request.UsersReq;
import com.kabarxxi.apps.response.CommonResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.AES;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private EditText confirmPassword;
    private EditText email;
    private EditText phoneNumber;
    private Button registerButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        email = (EditText) findViewById(R.id.email);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        registerButton = (Button) findViewById(R.id.registerButton);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                if(username.getText().toString().matches("")){
                    username.setError("Username tidak boleh kosong");
                    username.requestFocus();
                }

                if(password.getText().toString().matches("")){
                    password.setError("Password tidak boleh kosong");
                    password.requestFocus();
                }

                if(!password.getText().toString().equals(confirmPassword.getText().toString())){
                    password.setError("Password dan Konfirmasi Password harus sama");
                    password.requestFocus();
                }

                if(email.getText().toString().matches("")){
                    email.setError("Email tidak boleh kosong");
                    email.requestFocus();
                }

                if(phoneNumber.getText().toString().matches("")){
                    phoneNumber.setError("Nomor Hp tidak boleh kosong");
                    phoneNumber.requestFocus();
                }

                if(!username.getText().toString().equalsIgnoreCase("") && !password.getText().toString().equalsIgnoreCase("") && !email.toString().equalsIgnoreCase("") && !phoneNumber.toString().equalsIgnoreCase("") && password.getText().toString().equals(confirmPassword.getText().toString())){

                    String hashPassword = null;
                    try {
                        hashPassword = AES.encrypt(password.getText().toString());
                        System.out.println(hashPassword);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    UsersReq users = new UsersReq(email.getText().toString(), phoneNumber.getText().toString(), username.getText().toString(), hashPassword, 2 );
                    saveData(users);
                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    public void saveData(UsersReq users){

        final ProgressDialog progress = new ProgressDialog(RegisterActivity.this);
        progress.setMessage("Mohon Tunggu...");
        progress.setCancelable(false);
        progress.setIndeterminate(true);
        progress.show();

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<CommonResponse> call = service.registerUser(users);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    progress.dismiss();

                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert);

                    builder.setMessage(response.body().getMessage())
                            .setTitle("Info");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            clearForm((ViewGroup) findViewById(R.id.register_form));
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });

    }

    public void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText)view).setText("");
            }
        }
    }
}

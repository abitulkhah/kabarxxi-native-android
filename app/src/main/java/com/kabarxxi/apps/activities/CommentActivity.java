package com.kabarxxi.apps.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsCommentedAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.NewsCommented;
import com.kabarxxi.apps.request.NewsCommentReq;
import com.kabarxxi.apps.response.CommonResponse;
import com.kabarxxi.apps.response.NewsCommentResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NewsCommentedAdapter newsCommentedAdapter;
    private List<NewsCommented> newsCommentedList;
    private EditText commentET;
    private ImageButton submitButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_comment);
        commentET =  (EditText) findViewById(R.id.commentET);
        submitButton = (ImageButton) findViewById(R.id.submitButton);

        final Intent intent = getIntent();
        getData(intent.getLongExtra("newsId",0));

        SharedPreferences sharedPreferences = this.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        final Long userId = sharedPreferences.getString("userId", null) !=null ? Long.parseLong(sharedPreferences.getString("userId", null)) : 0;

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userId!=0){
                    postComment( userId, intent.getLongExtra("newsId",0));
                }else{
                    Toast.makeText(getApplicationContext(), "Anda harus login dahulu", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void getData(Long newsId){
        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<NewsCommentResponse> call = service.getNewsComment(newsId);
        call.enqueue(new retrofit2.Callback<NewsCommentResponse>() {
            @Override
            public void onResponse(Call<NewsCommentResponse> call, Response<NewsCommentResponse> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    newsCommentedList = response.body().getData();
                    newsCommentedAdapter = new NewsCommentedAdapter(getApplicationContext(), newsCommentedList );

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsCommentedAdapter);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
                    dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
                    recyclerView.addItemDecoration(dividerItemDecoration);
                    newsCommentedAdapter.notifyDataSetChanged();

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(), R.style.Theme_AppCompat_Light_Dialog_Alert);

                    builder.setMessage("Sesi anda telah habis,,silahkan login")
                            .setTitle("Info");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }

            @Override
            public void onFailure(Call<NewsCommentResponse> call, Throwable t) {

            }
        });
    }

    public void postComment(Long userId, final Long newsId){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);

        NewsCommentReq newsCommentReq = new NewsCommentReq(userId, newsId, commentET.getText().toString());

        Call<CommonResponse> call = service.submitComment(newsCommentReq);
        call.enqueue(new retrofit2.Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    commentET.setText(null);
                    getData(newsId);

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    AlertDialog.Builder builder = new AlertDialog.Builder(CommentActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert);

                    builder.setMessage("Sesi anda telah habis,,silahkan login")
                            .setTitle("Info");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(CommentActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

package com.kabarxxi.apps.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;

import java.net.URI;
import java.util.List;

public class DetailVideoActivity extends AppCompatActivity {


    private VideoView myVideoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_video);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();

        if (mediaControls == null) {
            mediaControls = new MediaController(DetailVideoActivity.this);
        }

        myVideoView = (VideoView) findViewById(R.id.video_view);

        progressDialog = new ProgressDialog(DetailVideoActivity.this);
        // set a title for the progress bar
        progressDialog.setTitle("Menampilkan Video");
        // set a message for the progress bar
        progressDialog.setMessage("Loading...");
        //set the progress bar not cancelable on users' touch
        progressDialog.setCancelable(false);
        // show the progress bar
        progressDialog.show();

        String content = intent.getStringExtra("content");
        String[] allContent = content.split("\"");

        try {

            if(allContent[1].contains("youtube")){

                String html = Constant.utubeIframe.replaceAll("youtubeUri", allContent[1]);
                WebView webview = (WebView) findViewById(R.id.utube_view);
                webview.loadData(html, "text/html", "UTF-8");
                webview.setVisibility(View.VISIBLE);
                webview.getSettings().setJavaScriptEnabled(true);
                webview.setWebChromeClient(new WebChromeClient());

            }else{

                myVideoView.setMediaController(mediaControls);
                myVideoView.setVideoPath(allContent[1]);
                myVideoView.requestFocus();
                //we also set an setOnPreparedListener in order to know when the video file is ready for playback
                myVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mediaPlayer) {
                        // close the progress bar and play the video
                        progressDialog.dismiss();
                        //if we have a position on savedInstanceState, the video playback should start from here
                        myVideoView.seekTo(position);
                        if (position == 0) {
                            myVideoView.start();
                        } else {
                            //if we come from a resumed activity, video playback will be paused
                            myVideoView.pause();
                        }
                    }
                });

            }

            //set the uri of the video to be play                                                                                                                                                                                                                                                                         ed
            progressDialog.dismiss();

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            progressDialog.dismiss();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

package com.kabarxxi.apps.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsBigAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.response.WPCategoriesResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.EndlessOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private RecyclerView recyclerView;
    private NewsBigAdapter newsBigAdapter;
    private Spinner searchCategorySP;
    private Button searchButton;
    private ProgressBar pBar;
    private String searchValue = "default";
    private long category = 0;
    private SearchView searchView;
    private FloatingActionButton arrowUpSearchNews;

    private List<Object> newsListTemp = new ArrayList<>();
    private List<Object> newsList = new ArrayList<>();
    private int page = 0;
    private int size = 10;
    private boolean resetData = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_big);
        searchCategorySP = (Spinner) findViewById(R.id.search_category_sp);
        searchButton = (Button) findViewById(R.id.search_news_button);
        arrowUpSearchNews = (FloatingActionButton) findViewById(R.id.arrow_up_searchnews);
        pBar = (ProgressBar) findViewById(R.id.pBar);

//        recyclerView.addOnScrollListener(scrollData());
        Constant.listWpCategories.add(0, new WPCategoriesResponse(0, "No Kategori", "Pilih Kategori", 0));
        ArrayAdapter<WPCategoriesResponse> adapter = new ArrayAdapter<WPCategoriesResponse>(this, android.R.layout.simple_spinner_item, Constant.listWpCategories);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        searchCategorySP.setAdapter(adapter);
        searchCategorySP.setOnItemSelectedListener(this);
        searchCategorySP.setVisibility(View.VISIBLE);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchValue = searchView.getQuery().toString();
                resetData = true;
                searchView.clearFocus();
                recyclerView.setVisibility(View.GONE);
                pBar.setVisibility(View.VISIBLE);
                getData();
            }
        });

        arrowUpSearchNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

    }

    public void getData(){

        page = page+1;

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.searchNewsByCategory(page, size, "desc", searchValue, category);
        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                if(response.isSuccessful()){

                    if(resetData){

                        newsList = new ArrayList<Object>(response.body());
                        resetData = false;

                        if(newsList.size() < 1){
                            Toast.makeText(getApplicationContext(), R.string.not_found, Toast.LENGTH_LONG).show();
                        }

                    }else{

                        newsListTemp = new ArrayList<Object>(response.body());
                        newsList.addAll(newsListTemp);

                    }

                    newsBigAdapter = new NewsBigAdapter(getApplicationContext(), newsList);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsBigAdapter);
                    recyclerView.setVisibility(View.VISIBLE);
                    newsBigAdapter.notifyDataSetChanged();
//                    dialog.dismiss();
                    pBar.setVisibility(View.GONE);


                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();

                    resetData = false;

                }

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
       super.onBackPressed();
       return true;
    }

    public boolean onCreateOptionsMenu( Menu menu) {

        getMenuInflater().inflate( R.menu.searchview_action_bar, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.requestFocus();
        searchView.setIconifiedByDefault(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchValue = query;
                getData();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchValue = newText;
                return true;
            }
        });

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        resetData = true;
        parent.getSelectedItem().toString();
        category = Constant.listWpCategories.get(position).getId();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private EndlessOnScrollListener scrollData() {

        return new EndlessOnScrollListener() {
            @Override
            public void onLoadMore() {

                getData();

            }
        };
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}

package com.kabarxxi.apps.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.kabarxxi.apps.MainActivity;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.response.LoginUsersResponse;
import com.kabarxxi.apps.response.UsersResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.AES;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button loginButtton;
    private ProgressBar loginSpinner;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginButtton = (Button) findViewById(R.id.loginButton);


        loginButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(username.getText().toString().matches("")){
                    username.setError("Username tidak boleh kosong");
                    username.requestFocus();
                }

                if(password.getText().toString().matches("")){
                    password.setError("Password tidak boleh kosong");
                    password.requestFocus();
                }

                postLogin(username.getText().toString(), password.getText().toString());

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    public void postLogin(final String username, String password){


        progress = new ProgressDialog(LoginActivity.this);
        progress.setMessage("Mohon Tunggu...");
        progress.setCancelable(false);
        progress.setIndeterminate(true);
        progress.show();

        String hashPassword = null;

        try {
            hashPassword = AES.encrypt(password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<LoginUsersResponse> call = service.postLogin(username, hashPassword, "password");
        call.enqueue(new Callback<LoginUsersResponse>() {
            @Override
            public void onResponse(Call<LoginUsersResponse> call, Response<LoginUsersResponse> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    Config.TOKEN = response.body().getAccess_token();
                    getUsersDetails(username, response.body().getAccess_token());

                }

                if(response.code()==400){

                    progress.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.Theme_AppCompat_Light_Dialog_Alert);

                    builder.setMessage("Username atau password salah")
                            .setTitle("Info");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }

            }

            @Override
            public void onFailure(Call<LoginUsersResponse> call, Throwable t) {

            }
        });
    }

    public void getUsersDetails(final String username, final String token){
        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<UsersResponse> call = service.getUsersByUsername(username);
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("token", token);
                    editor.putString("userId", response.body().getData().getId().toString());
                    editor.putString("username", username);
                    editor.putString("email", response.body().getData().getEmail());
                    editor.putString("phoneNumber", response.body().getData().getPhoneNumber());

                    editor.commit();

                    progress.dismiss();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {

            }
        });
    }

    public void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText)view).setText("");
            }
        }
    }
}


package com.kabarxxi.apps.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.RelatedNewsAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.models.RelatedNews;
import com.kabarxxi.apps.response.CommonResponse;
import com.kabarxxi.apps.response.NewsDetailResponse;
import com.kabarxxi.apps.response.NewsResponse;
import com.kabarxxi.apps.response.WPAuthorResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class DetailNewsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView detailNewsContent;
    private RecyclerView recyclerView;
    private RelatedNewsAdapter relatedNewsAdapter;
    private List<RelatedNews> relatedNewsList;
    private TextView newsDetailTitle;
    private TextView creator;
    private TextView newsDetailTime;
    private ImageView imageNewsDetail;
    private String newsTitle = null;
    private String imageUri = null;
    private long newsId = 0;
    private int page = 1;
    private int size = 5;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);

        newsDetailTitle =  (TextView) findViewById(R.id.detailNewsTitle);
        creator =  (TextView) findViewById(R.id.creator);
        newsDetailTime =  (TextView) findViewById(R.id.detailNewsTime);
        imageNewsDetail =  (ImageView) findViewById(R.id.detailImageNews);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_related_news);

        final Intent intent = getIntent();

//        getDetailAuthor(intent.getLongExtra("authorId", 0));
        getImage(intent.getStringExtra("imageUri"));
        newsDetailTitle.setText(intent.getStringExtra("newsTitle"));
        creator.setText(intent.getStringExtra("releaseBy")+" - " +intent.getStringExtra("categoryName") );
        newsDetailTime.setText(intent.getStringExtra("newsTime"));

        newsId = intent.getLongExtra("newsId", 0);
        newsTitle = intent.getStringExtra("newsTitle").replaceAll("[ ,;]","-");
        imageUri = intent.getStringExtra("imageUri").replaceAll("[ ,;]","-");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        detailNewsContent = (TextView) findViewById(R.id.detailNewsContent);
        String description = intent.getStringExtra("content");

        if(description!=null && description!="null"){
            detailNewsContent.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                detailNewsContent.setText(Html.fromHtml(description,Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
            } else {
                detailNewsContent.setText(Html.fromHtml(description));
            }

            detailNewsContent.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
            detailNewsContent.setLineSpacing(10f, 1.2f);
            detailNewsContent.setTextSize(16.0f);

        }else{

            getNewsDetail(intent.getLongExtra("newsId", 0));

        }


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getRelatedNews(intent.getStringExtra("keyword"));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent commentIntent = new Intent(getApplicationContext(), CommentActivity.class);
                commentIntent.putExtra("newsId", intent.getLongExtra("newsId", 0));
                startActivity(commentIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.detail_news_action_bar, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.newsDetailShare:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Berita Share");
                intent.putExtra(Intent.EXTRA_TEXT, Config.BASE_URL+"/"+newsTitle);
                startActivity(Intent.createChooser(intent, "Share Link"));
                return true;
            case R.id.smallResize:
                detailNewsContent.setTextSize(16.0f);
                return true;
            case R.id.mediumResize:
                detailNewsContent.setTextSize(20.0f);
                return true;
            case R.id.bigResize:
                detailNewsContent.setTextSize(24.0f);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void getDetailAuthor(final long authorId){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPAuthorResponse> call = service.getDetailAuthor(authorId);
        call.enqueue(new retrofit2.Callback<WPAuthorResponse>() {
            @Override
            public void onResponse(Call<WPAuthorResponse> call, Response<WPAuthorResponse> response) {

                if(response.isSuccessful()){

                    WPAuthorResponse author = response.body();
                    System.out.println(response.body());
                    creator.setText(author.getName());
                    creator.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<WPAuthorResponse> call, Throwable t) {

            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void directPage(Class activityClass){
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    public void getImage(final String uri){
        Picasso mPicasso = Picasso.with(this);
        mPicasso.setIndicatorsEnabled(false);
        mPicasso.with(this)
                .load(uri)
                .fit()
                .into(imageNewsDetail, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                    }
                });
    }

    public void getRelatedNews(final String keyword){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getLatestNews(page, size, "desc");
        call.enqueue(new retrofit2.Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    List<WPNewsResponse> newsList = response.body();
//                    List<News> newsList = new ArrayList<>();
                    relatedNewsAdapter = new RelatedNewsAdapter(getApplicationContext(), newsList);
                    relatedNewsAdapter.notifyDataSetChanged();

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(relatedNewsAdapter);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
                    dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
                    recyclerView.addItemDecoration(dividerItemDecoration);

                }

                if(response.code()==401){

                    Config.TOKEN = null;
                    getRelatedNews(keyword);

                }

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });
    }

    public void getNewsDetail(final long id){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPNewsResponse> call = service.getDetailNews(id);
        call.enqueue(new retrofit2.Callback<WPNewsResponse>() {
            @Override
            public void onResponse(Call<WPNewsResponse> call, Response<WPNewsResponse> response) {

                if(response.isSuccessful()){

                    WPNewsResponse news = response.body();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        detailNewsContent.setText(Html.fromHtml(news.getContent().getRendered(),Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        detailNewsContent.setText(Html.fromHtml(news.getContent().getRendered()));
                    }

                    detailNewsContent.setVisibility(View.VISIBLE);

                }

                if(response.code()==401){

                    Config.TOKEN = null;
                    getNewsDetail(id);

                }
            }

            @Override
            public void onFailure(Call<WPNewsResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ((TextView) view).setTextColor(Color.TRANSPARENT);
        ((TextView)view).setText(null);
        float newSize =  0.0f;

        if(position==0){
            newSize = 14.0f;
        }else if(position==1){
            newSize = 18.0f;
        }else{
            newSize = 24.0f;
        }

        detailNewsContent.setTextSize(newSize);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

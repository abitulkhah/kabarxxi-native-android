package com.kabarxxi.apps.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NotificationAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.Notification;
import com.kabarxxi.apps.response.NotificationResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private List<Notification> notificationList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_notification);

        getData();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void getData(){
        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<NotificationResponse> call = service.getPushNotification();
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                if(response.isSuccessful()){

                    notificationList = response.body().getData() ;
                    notificationAdapter = new NotificationAdapter(getApplicationContext(), notificationList );
                    notificationAdapter.notifyDataSetChanged();

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(notificationAdapter);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
                    dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
                    recyclerView.addItemDecoration(dividerItemDecoration);

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();
                }

            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                System.out.println("errror");
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

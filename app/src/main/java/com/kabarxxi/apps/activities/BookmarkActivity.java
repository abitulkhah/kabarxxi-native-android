package com.kabarxxi.apps.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsSmallAdapter;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.models.NewsBookmark;
import com.kabarxxi.apps.response.WPNewsResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

;

public class BookmarkActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private NewsSmallAdapter newsSmallAdapter;
    private List<NewsBookmark> bookmarkList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_bookmark);

        SharedPreferences sharedPreferences = this.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        String bookmarkData = sharedPreferences.getString("bookmarkList", "");

        Gson gson = new Gson();
        List<WPNewsResponse> newsList;

        Type type = new TypeToken<List<WPNewsResponse>>() {}.getType();

        newsList = gson.fromJson(bookmarkData, type);

        if(newsList == null){
            newsList = new ArrayList<>();
        }

        newsSmallAdapter = new NewsSmallAdapter(this,  new ArrayList<Object>(newsList));

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(newsSmallAdapter);
        newsSmallAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    
}

package com.kabarxxi.apps.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.AdvertiserAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.response.AdvertiserResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdvertiserFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdvertiserAdapter advertiserBigAdapter;
    private List<Object> advertiserList = new ArrayList<>();
    private List<Object> advertiserListTemp = new ArrayList<>();
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpAdvertiser;
    private ProgressBar progressBar;
    private boolean isLoaded = false;

    private int page = 0;
    private int size = 10;

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    private ProgressBar advertiserProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advertiser, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_big);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerAdvertiser);
        // Setup refresh listener which triggers new data loading
        arrowUpAdvertiser = (FloatingActionButton) view.findViewById(R.id.arrow_up_advertiser);
        advertiserProgressBar = (ProgressBar) view.findViewById(R.id.pBarAdvertiserNews);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_advertiser);

        arrowUpAdvertiser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 0;
                getData();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == advertiserList.size() - 1) {
                    getData();
                }
            }
        });

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && !isLoaded){
            getData();
            isLoaded = true;
        }
    }


    public void getData(){

        advertiserProgressBar.setVisibility(View.VISIBLE);

        page = page+1;

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getAdvertiser(page, size, "desc", Constant.ADVERTISER_CATEGORY);
        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                if(response.isSuccessful()){

                    advertiserListTemp = new ArrayList<Object>(response.body());
                    advertiserList.addAll(advertiserListTemp);

                    advertiserBigAdapter = new AdvertiserAdapter(getContext(), advertiserList);

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(advertiserBigAdapter);
                    advertiserBigAdapter.notifyDataSetChanged();
                    swipeContainer.setRefreshing(false);

                }

                if(response.code()==401){

                    Config.TOKEN = null;
                    page = -1;
                    getData();

                }

                advertiserProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });
    }

    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (advertiserList.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad: mNativeAds) {
            advertiserList.add(index, ad);
            index = index + offset;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

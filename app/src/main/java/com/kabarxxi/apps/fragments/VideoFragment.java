package com.kabarxxi.apps.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsVideoAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.models.NewsVideo;
import com.kabarxxi.apps.response.NewsVideoResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VideoFragment extends Fragment {

    private RecyclerView recyclerView;
    private NewsVideoAdapter newsVideoAdapter;
    private SwipeRefreshLayout swipeContainer;
    private int page = 1;
    private int size = 10;
    private String order = "desc";

    private List<Object> newsVideoList =  new ArrayList<>();
    private List<Object> newsVideoListTemp =  new ArrayList<>();

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_news_video);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerVideo);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }
        });
        getData();

        return view;
    }

    public void getData(){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getNewsVideo(page, size, order, Constant.VIDEO_CATEGORY);

        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                System.out.println(response);

                if(response.isSuccessful()){

                    List<WPNewsResponse> newsVideoList = response.body();
                    newsVideoAdapter = new NewsVideoAdapter(getContext(), newsVideoList);

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsVideoAdapter);
//                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
//                    dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
//                    recyclerView.addItemDecoration(dividerItemDecoration);

                    newsVideoAdapter.notifyDataSetChanged();

                    swipeContainer.setRefreshing(false);

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();

                }


            }



            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });
    }


    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (newsVideoList.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad: mNativeAds) {
            newsVideoList.add(index, ad);
            index = index + offset;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

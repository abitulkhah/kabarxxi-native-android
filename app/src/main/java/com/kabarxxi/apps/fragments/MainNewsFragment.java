package com.kabarxxi.apps.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsBigAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.response.NewsResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainNewsFragment extends Fragment {

    private RecyclerView recyclerView;
    private NewsBigAdapter newsBigAdapter;
    private  List<Object> newsList = new ArrayList<>();
    private  List<Object> newsListTemp = new ArrayList<>();
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpMainNews;
    private boolean isLoaded = false;

    private int page = 0;
    private int size = 10;

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    private ProgressBar mainProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_news, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_big);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerMainNews);
        arrowUpMainNews = (FloatingActionButton) view.findViewById(R.id.arrow_up_mainnews);
        mainProgressBar = (ProgressBar) view.findViewById(R.id.pBarMainNews);

        arrowUpMainNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 0;
                getData();
            }
        });


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && !isLoaded){
            getData();
            isLoaded = true;
        }
    }

    public void getData(){

        mainProgressBar.setVisibility(View.VISIBLE);

        page = page+1;

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getMainNews(page, size, "desc", Constant.ADVERTISER_CATEGORY);
        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                if(response.isSuccessful()){

                    newsListTemp = new ArrayList<Object>(response.body());
                    newsList.addAll(newsListTemp);
                    newsBigAdapter = new NewsBigAdapter(getContext(), newsList);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsBigAdapter);
                    newsBigAdapter.notifyDataSetChanged();
                    swipeContainer.setRefreshing(false);

                }

                if(response.code()==401){

                    Config.TOKEN = null;
                    page = -1;
                    getData();

                }

                mainProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });
    }

    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (newsList.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad: mNativeAds) {
            newsList.add(index, ad);
            index = index + offset;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

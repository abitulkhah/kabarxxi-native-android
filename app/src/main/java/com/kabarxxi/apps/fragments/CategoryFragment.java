package com.kabarxxi.apps.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsCategoryAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.models.NewsCategory;
import com.kabarxxi.apps.response.NewsCategoryResponse;
import com.kabarxxi.apps.response.WPCategoriesResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.AutoColumn;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private NewsCategoryAdapter newsCategoryAdapter;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpCategory;
    private int page = 1;
    private int size = 100;
    private String order = "asc";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_news_category);
        arrowUpCategory = (FloatingActionButton) view.findViewById(R.id.arrow_up_category);

        arrowUpCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        getData();

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerCategory);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }
        });

        return view;
    }

    public void getData(){
        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPCategoriesResponse>> call = service.getCategories(page, size, order);
        call.enqueue(new Callback<List<WPCategoriesResponse>>() {
            @Override
            public void onResponse(Call<List<WPCategoriesResponse>> call, Response<List<WPCategoriesResponse>> response) {

                if(response.isSuccessful()){

                    List<WPCategoriesResponse> newsCategoryList = response.body();
                    Constant.listWpCategories = response.body();
                    newsCategoryAdapter = new NewsCategoryAdapter(getContext(), newsCategoryList);
                    newsCategoryAdapter.notifyDataSetChanged();

                    Integer countSpan = AutoColumn.calculateNoOfColumns(getContext(), 120);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsCategoryAdapter);
                    swipeContainer.setRefreshing(false);
                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();

                }
            }

            @Override
            public void onFailure(Call<List<WPCategoriesResponse>> call, Throwable t) {
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

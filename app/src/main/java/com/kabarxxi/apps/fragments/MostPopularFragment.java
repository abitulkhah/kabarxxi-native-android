package com.kabarxxi.apps.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.adapters.NewsSmallAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.config.Constant;
import com.kabarxxi.apps.response.NewsResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MostPopularFragment extends Fragment {
    
    private RecyclerView recyclerView;
    private NewsSmallAdapter newsSmallAdapter;
    private List<Object> newsListTemp = new ArrayList<>();
    private List<Object> newsList = new ArrayList<>();
    private int page = 0;
    private int size = 10;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpMostPopular;
    private boolean isLoaded = false;

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    private ProgressBar mostPopularProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_most_popular, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_small);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerMostPopular);
        arrowUpMostPopular = (FloatingActionButton) view.findViewById(R.id.arrow_up_mostpopular);
        mostPopularProgressBar = (ProgressBar) view.findViewById(R.id.pBarPopularNews);

        arrowUpMostPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 0;
                getData();
            }
        });

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && !isLoaded){
            getData();
            isLoaded = true;
        }
    }

    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (newsList.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad: mNativeAds) {
            newsList.add(index, ad);
            index = index + offset;
        }
    }

    public void getData(){

        mostPopularProgressBar.setVisibility(View.VISIBLE);

        page = page+1;

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getPopularNews(page, size, "desc", Constant.ADVERTISER_CATEGORY);
        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                if(response.isSuccessful()){

                    newsListTemp = new ArrayList<Object>(response.body());
                    newsList.addAll(newsListTemp);

                    newsSmallAdapter = new NewsSmallAdapter(getContext(), newsList);

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsSmallAdapter);
                    swipeContainer.setRefreshing(false);

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();

                }

                mostPopularProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

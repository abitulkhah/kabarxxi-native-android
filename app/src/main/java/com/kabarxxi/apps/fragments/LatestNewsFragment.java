package com.kabarxxi.apps.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.DetailNewsActivity;
import com.kabarxxi.apps.adapters.NewsSmallAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.response.WPMediaDetailResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.LocaleDateFormat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LatestNewsFragment extends Fragment {

    private RecyclerView recyclerView;
    private NewsSmallAdapter newsSmallAdapter;
    private CardView newsTopCardView;
    private SwipeRefreshLayout swipeContainer;
    private ImageView newsTopImage;
    private TextView newsTopTitle;
    private TextView newsTopTime;
    private FloatingActionButton arrowUpLatestNews;

    private List<Object> newsListTemp = new ArrayList<>();
    private List<Object> newsList = new ArrayList<>();

    private Integer indexNewsTop = -1;
    private int page = 1;
    private int size = 10;
    private int totalItemCount = 0;
    private int previousTotalCount = 10;
    private boolean isLoading = false;
    private ProgressBar progressBar;
    private ScrollView scrollView;

    private View view;

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 5;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();

    private AdView mAdView;
    private ProgressBar pBarLatestNews;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_latest_news, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_small);
        newsTopCardView = (CardView) view.findViewById(R.id.news_top_card_view);
        progressBar = (ProgressBar) view.findViewById(R.id.item_progress_bar);
        pBarLatestNews = (ProgressBar) view.findViewById(R.id.pBarLatestNews);
        scrollView = (ScrollView) view.findViewById(R.id.scroll_view_latest_news);

        scrollView.setSmoothScrollingEnabled(true);
        scrollView.getViewTreeObserver()
                .addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (scrollView.getChildAt(0).getBottom() <= (scrollView.getHeight() + scrollView.getScrollY())) {

                            if(!isLoading){
                                getData();
                                progressBar.setVisibility(View.VISIBLE);
                            }

                        } else {
                            //scroll view is not at bottom
                        }
                    }
                });
        pBarLatestNews.setVisibility(View.VISIBLE);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        arrowUpLatestNews = (FloatingActionButton) view.findViewById(R.id.arrow_up_latestnews);

        arrowUpLatestNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.scrollTo(5,10);
            }
        });


        getData();

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerLatestNews);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }
        });

        newsTopImage = (ImageView) view.findViewById(R.id.newsTopImage);
        newsTopTitle = (TextView) view.findViewById(R.id.newsTopTitle);
        newsTopTime = (TextView) view.findViewById(R.id.newsTopTime);

        newsTopCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocaleDateFormat localeDateFormat = new LocaleDateFormat();
                final WPNewsResponse newsTopData = (WPNewsResponse) newsList.get(indexNewsTop);
                final String dateNews = localeDateFormat.getTimeAgo(newsTopData.getDate());

                Intent intent = new Intent(getActivity(), DetailNewsActivity.class);
                intent.putExtra("newsId", newsTopData.getId());
                intent.putExtra("authorId", newsTopData.getAuthor());
                intent.putExtra("newsTitle", newsTopData.getTitle().getRendered());
                intent.putExtra("newsTime", dateNews);
                intent.putExtra("content", newsTopData.getContent().getRendered());
                intent.putExtra("imageUri", newsTopData.getMedia_details().getSizes().getMedium().getSource_url());
                getActivity().startActivity(intent);

            }
        });

        return view;
    }

    public void getData(){

        isLoading = true;

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<List<WPNewsResponse>> call = service.getLatestNews(page, size, "desc");
        call.enqueue(new Callback<List<WPNewsResponse>>() {
            @Override
            public void onResponse(Call<List<WPNewsResponse>> call, Response<List<WPNewsResponse>> response) {

                if(response.isSuccessful()){

                    newsListTemp = new ArrayList<Object>(response.body());
                    totalItemCount = totalItemCount+newsListTemp.size();
                    newsList.addAll(newsListTemp);

                    if(page==1){
                        setTopImage(newsList);
                    }

                    newsSmallAdapter = new NewsSmallAdapter(getContext(), newsList);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(newsSmallAdapter);

                    newsSmallAdapter.notifyDataSetChanged();
                    recyclerView.setFocusable(true);
                    scrollView.setFocusable(true);
                    swipeContainer.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    pBarLatestNews.setVisibility(View.GONE);

                    page = page+1;
                    isLoading = false;

                }

                if(response.code()==401){

                    Config.TOKEN = null;
                    getData();

                }

            }

            @Override
            public void onFailure(Call<List<WPNewsResponse>> call, Throwable t) {
                pBarLatestNews.setVisibility(View.GONE);
            }
        });
    }

    public void setTopImage(List<Object> data){

        indexNewsTop = 0;
        final WPNewsResponse news = (WPNewsResponse) data.get(indexNewsTop);
        Picasso mPicasso = Picasso.with(getContext());
        mPicasso.setIndicatorsEnabled(false);
        this.getDetailImage(news.getFeatured_media(), news);
        newsTopTitle.setText(news.getTitle().getRendered());
        LocaleDateFormat localeDateFormat = new LocaleDateFormat();
        String dateNews = localeDateFormat.getTimeAgo(news.getDate());
        newsTopTime.setText(dateNews);
        newsTopCardView.setVisibility(View.VISIBLE);

    }

    public void getDetailImage(int imageId, final WPNewsResponse newsResponse){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(imageId);
        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    newsResponse.setMedia_details(wpMediaDetailResponse.getMedia_details());

                    Picasso mPicasso = Picasso.with(getContext());
                    mPicasso.with(getContext())
                            .load(wpMediaDetailResponse.getMedia_details().getSizes().getMedium().getSource_url())
                            .fit()
                            .into(newsTopImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

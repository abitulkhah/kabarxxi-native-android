package com.kabarxxi.apps.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.BookmarkActivity;
import com.kabarxxi.apps.activities.LoginActivity;
import com.kabarxxi.apps.activities.NotificationActivity;
import com.kabarxxi.apps.activities.RegisterActivity;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.utils.CountDrawable;


public class ProfileFragment extends Fragment {

    private Button logginButon;
    private Button logoutButon;
    private TextView registerTV;
    private LinearLayout afterProfile;
    private LinearLayout preProfile;
    private TextView nameTV;
    private TextView emailTV;
    private TextView phoneTV;
    private MenuItem notifMenuItem;

    private String countNotification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        logginButon = (Button) view.findViewById(R.id.loginPageButton);
        logoutButon = (Button) view.findViewById(R.id.logoutButton);
        registerTV = (TextView) view.findViewById(R.id.registerTV);
        preProfile = (LinearLayout) view.findViewById(R.id.preProfile);
        afterProfile = (LinearLayout) view.findViewById(R.id.afterProfile);
        nameTV = (TextView) view.findViewById(R.id.nameTV);
        emailTV = (TextView) view.findViewById(R.id.emailTV);
        phoneTV = (TextView) view.findViewById(R.id.phoneTV);

        logginButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directPage(LoginActivity.class);
            }
        });
        registerTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directPage(RegisterActivity.class);
            }
        });

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", null);
        String username = sharedPreferences.getString("username", "");
        String email = sharedPreferences.getString("email", "");
        String phone = sharedPreferences.getString("phoneNumber", "");

        if(token!=null){
            preProfile.setVisibility(View.GONE);
            afterProfile.setVisibility(View.VISIBLE);
            nameTV.setText(username);
            emailTV.setText(email);
            phoneTV.setText(phone);
        }

        logoutButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                Config.TOKEN = null;
                preProfile.setVisibility(View.VISIBLE);
                afterProfile.setVisibility(View.GONE);
            }
        });

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.profile_action_bar, menu);
        notifMenuItem = menu.findItem(R.id.notificationProfile);
        SharedPreferences notificationPref = getContext().getSharedPreferences("NotificationPref", Context.MODE_PRIVATE);
        countNotification = notificationPref.getString("countNotification", "30");

        setCount(getContext(),countNotification);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        System.out.println(R.id.notificationProfile);
        System.out.println(id);
        System.out.println(R.id.bookmarkProfile);
        //noinspection SimplifiableIfStatement
        if (id == R.id.bookmarkProfile) {
            directPage(BookmarkActivity.class);
        }
        if (id == R.id.notificationProfile) {
            directPage(NotificationActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    public void directPage(Class activityClass){
        Intent intent = new Intent(getActivity(), activityClass);
        getActivity().startActivity(intent);
    }

    public void setCount(Context context, String count) {
        LayerDrawable icon = (LayerDrawable) notifMenuItem.getIcon();

        CountDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_notif_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_notif_count, badge);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}

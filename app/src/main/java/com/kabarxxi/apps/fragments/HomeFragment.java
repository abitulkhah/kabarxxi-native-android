package com.kabarxxi.apps.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.SearchActivity;
import com.kabarxxi.apps.adapters.TabAdapter;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.response.CountNotificationResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {


    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(null);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setIcon(R.drawable.logo_kabar);

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getFragmentManager());
        adapter.addFragment(new LatestNewsFragment(), "Terbaru");
        adapter.addFragment(new MainNewsFragment(), "Berita Utama");
        adapter.addFragment(new AdvertiserFragment(), "Adversiter");
        adapter.addFragment(new MostPopularFragment(), "Paling Popular");
        adapter.addFragment(new OpinionFragment(), "Opini");
        adapter.addFragment(new NewsTipsFragment(), "Info dan Tips");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(10);
        tabLayout.setupWithViewPager(viewPager);

        getData();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.home_action_bar, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {

            // Do something
            Intent intent =  new Intent(getActivity(), SearchActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.navigation_category) {

            // Do something
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getData() {


        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<CountNotificationResponse> call = service.getCountNotification();
        call.enqueue(new Callback<CountNotificationResponse>() {
            @Override
            public void onResponse(Call<CountNotificationResponse> call, Response<CountNotificationResponse> response) {

                if (response.isSuccessful()) {

                    long countNotification = response.body().getData();

                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("NotificationPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("countNotification",""+countNotification);

                    editor.commit();

                }

                if(response.code()==401){

                    Config.TOKEN = null;

                    getData();

                }

            }

            @Override
            public void onFailure(Call<CountNotificationResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

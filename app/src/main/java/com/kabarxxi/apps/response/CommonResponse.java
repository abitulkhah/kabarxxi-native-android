package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public CommonResponse(String message, Integer status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

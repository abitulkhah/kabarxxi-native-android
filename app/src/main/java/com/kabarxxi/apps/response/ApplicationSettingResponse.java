package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.ApplicationSetting;

import java.util.List;

public class ApplicationSettingResponse {

    @SerializedName("data")
    private List<ApplicationSetting> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public ApplicationSettingResponse(List<ApplicationSetting> data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public List<ApplicationSetting> getData() {
        return data;
    }

    public void setData(List<ApplicationSetting> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

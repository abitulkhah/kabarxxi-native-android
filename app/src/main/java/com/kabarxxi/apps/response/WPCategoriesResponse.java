package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WPCategoriesResponse implements Serializable {

    @SerializedName("id")
    private long id;

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @SerializedName("parent")
    private long parent;

    public WPCategoriesResponse(long id, String description, String name, long parent) {
        this.id = id;
        this.description = description;
        this.name = name;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {

        return getName();

    }
}

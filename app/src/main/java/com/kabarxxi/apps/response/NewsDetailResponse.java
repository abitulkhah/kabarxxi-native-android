package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.News;

public class NewsDetailResponse {

    @SerializedName("data")
    private News data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public NewsDetailResponse(News data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public News getData() {
        return data;
    }

    public void setData(News data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

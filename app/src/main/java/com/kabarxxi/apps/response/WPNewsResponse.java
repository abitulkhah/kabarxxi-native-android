package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.WPMediaDetail;
import com.kabarxxi.apps.models.WpNewsContent;
import com.kabarxxi.apps.models.WpNewsTitle;

import java.util.Date;


public class WPNewsResponse {

    @SerializedName("id")
    private long id;

    @SerializedName("date")
    private Date date;

    @SerializedName("slug")
    private String slug;

    @SerializedName("status")
    private String status;

    @SerializedName("type")
    private String type;

    @SerializedName("link")
    private String link;

    @SerializedName("title")
    private WpNewsTitle title;

    @SerializedName("author")
    private long author;

    @SerializedName("featured_media")
    private Integer featured_media;

    @SerializedName("categories")
    private Integer[] categories;

    @SerializedName("comment_status")
    private String comment_status;

    @SerializedName("content")
    private WpNewsContent content;

    @SerializedName("media_details")
    private WPMediaDetail media_details;

    public WPNewsResponse(long id, Date date, String slug, String status, String type, String link, WpNewsTitle title, long author, Integer featured_media, Integer[] categories, String comment_status, WpNewsContent content, WPMediaDetail media_details) {
        this.id = id;
        this.date = date;
        this.slug = slug;
        this.status = status;
        this.type = type;
        this.link = link;
        this.title = title;
        this.author = author;
        this.featured_media = featured_media;
        this.categories = categories;
        this.comment_status = comment_status;
        this.content = content;
        this.media_details = media_details;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public WpNewsTitle getTitle() {
        return title;
    }

    public void setTitle(WpNewsTitle title) {
        this.title = title;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public Integer getFeatured_media() {
        return featured_media;
    }

    public void setFeatured_media(Integer featured_media) {
        this.featured_media = featured_media;
    }

    public Integer[] getCategories() {
        return categories;
    }

    public void setCategories(Integer[] categories) {
        this.categories = categories;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public WpNewsContent getContent() {
        return content;
    }

    public void setContent(WpNewsContent content) {
        this.content = content;
    }

    public WPMediaDetail getMedia_details() {
        return media_details;
    }

    public void setMedia_details(WPMediaDetail media_details) {
        this.media_details = media_details;
    }
}

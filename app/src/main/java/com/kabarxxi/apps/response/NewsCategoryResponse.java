package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.NewsCategory;

import java.util.List;

public class NewsCategoryResponse {
    @SerializedName("data")
    private List<NewsCategory> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public NewsCategoryResponse(List<NewsCategory> data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

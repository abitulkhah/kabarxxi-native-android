package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;

public class CountNotificationResponse {
    @SerializedName("data")
    private long data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public CountNotificationResponse(long data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

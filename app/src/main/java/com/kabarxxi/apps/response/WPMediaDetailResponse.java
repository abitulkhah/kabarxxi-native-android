package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.WPMediaDetail;
import com.kabarxxi.apps.models.WpNewsTitle;

import java.util.Date;

public class WPMediaDetailResponse {

    @SerializedName("id")
    private Integer id;

    @SerializedName("date")
    private Date date;

    @SerializedName("slug")
    private String slug;

    @SerializedName("status")
    private String status;

    @SerializedName("type")
    private String type;

    @SerializedName("link")
    private String link;

    @SerializedName("title")
    private WpNewsTitle title;

    @SerializedName("media_details")
    private WPMediaDetail media_details;

    public WPMediaDetailResponse(Integer id, Date date, String slug, String status, String type, String link, WpNewsTitle title, WPMediaDetail media_details) {
        this.id = id;
        this.date = date;
        this.slug = slug;
        this.status = status;
        this.type = type;
        this.link = link;
        this.title = title;
        this.media_details = media_details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public WpNewsTitle getTitle() {
        return title;
    }

    public void setTitle(WpNewsTitle title) {
        this.title = title;
    }

    public WPMediaDetail getMedia_details() {
        return media_details;
    }

    public void setMedia_details(WPMediaDetail media_details) {
        this.media_details = media_details;
    }
}

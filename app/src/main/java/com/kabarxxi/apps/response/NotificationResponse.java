package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.Notification;

import java.util.List;

public class NotificationResponse {
    @SerializedName("data")
    private List<Notification> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public NotificationResponse(List<Notification> data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public List<Notification> getData() {
        return data;
    }

    public void setData(List<Notification> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

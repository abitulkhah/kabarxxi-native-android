package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.Advertiser;
import com.kabarxxi.apps.models.RelatedNews;

import java.util.List;

public class AdvertiserResponse {

    @SerializedName("data")
    private List<Advertiser> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public List<Advertiser> getData() {
        return data;
    }

    public void setData(List<Advertiser> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

package com.kabarxxi.apps.response;

import com.google.gson.annotations.SerializedName;
import com.kabarxxi.apps.models.RelatedNews;
import com.kabarxxi.apps.models.Users;

import java.util.List;

public class UsersResponse {

    @SerializedName("data")
    private Users data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public UsersResponse(Users data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public Users getData() {
        return data;
    }

    public void setData(Users data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

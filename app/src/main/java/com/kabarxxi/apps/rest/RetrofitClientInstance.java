package com.kabarxxi.apps.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kabarxxi.apps.config.Config;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {

        if (retrofit == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor())
                    .build();

            String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
            Gson gson = new GsonBuilder()
                    .setDateFormat(DATE_FORMAT)
                    .create();

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }
        return retrofit;
    }

//    public static Retrofit getRetrofitInstance(Context mContext) {
//        if (retrofit == null) {
//            File httpCacheDirectory = new File(mContext.getCacheDir(), "httpCache");
//            Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);
//            OkHttpClient client = new OkHttpClient.Builder()
//                    .cache(cache)
//                    .addInterceptor(new BasicAuthInterceptor())
//                    .build();
//
//            retrofit = new retrofit2.Retrofit.Builder()
//                    .baseUrl(Config.BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(client)
//                    .build();
//        }
//        return retrofit;
//    }
}

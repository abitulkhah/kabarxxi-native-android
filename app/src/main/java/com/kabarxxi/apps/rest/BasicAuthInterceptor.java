package com.kabarxxi.apps.rest;

import com.kabarxxi.apps.config.Config;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BasicAuthInterceptor implements Interceptor {

    private String credentials;
    private String user = "kabarxxi-client-portal";
    private String password = "VlVjNWVXUkhSbk5WZWs1cVkycE9NRWt3Um5waFJFVjVUVlJWTVUxcVdUSk5lbXQ1VFVSVk1VNW5QVDA9VlVjNWVXUkhSbk5WZWs1cVkycE9NRQ==";

    public BasicAuthInterceptor() {
        credentials = Credentials.basic(user, password);
    }


    @Override
    public Response intercept(Chain chain) throws IOException {


        Request request = chain.request();

        if(Config.TOKEN!=null && request.url().url().toString().contains("oauth/token") == false){
            credentials = "Bearer "+Config.TOKEN;
        }else{
            credentials = Credentials.basic(user, password);
        }

        Request authenticatedRequest = request.newBuilder()
                .header("Authorization", credentials).build();
        return chain.proceed(authenticatedRequest);
    }

}

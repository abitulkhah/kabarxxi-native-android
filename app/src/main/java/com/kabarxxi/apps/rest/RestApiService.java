package com.kabarxxi.apps.rest;

import com.kabarxxi.apps.request.NewsCommentReq;
import com.kabarxxi.apps.request.UsersReq;
import com.kabarxxi.apps.response.AdvertiserResponse;
import com.kabarxxi.apps.response.ApplicationSettingResponse;
import com.kabarxxi.apps.response.CommonResponse;
import com.kabarxxi.apps.response.CountNotificationResponse;
import com.kabarxxi.apps.response.LoginUsersResponse;
import com.kabarxxi.apps.response.NewsCategoryResponse;
import com.kabarxxi.apps.response.NewsCommentResponse;
import com.kabarxxi.apps.response.NewsDetailResponse;
import com.kabarxxi.apps.response.NewsResponse;
import com.kabarxxi.apps.response.NewsVideoResponse;
import com.kabarxxi.apps.response.NotificationResponse;
import com.kabarxxi.apps.response.UsersResponse;
import com.kabarxxi.apps.response.WPAuthorResponse;
import com.kabarxxi.apps.response.WPCategoriesResponse;
import com.kabarxxi.apps.response.WPMediaDetailResponse;
import com.kabarxxi.apps.response.WPNewsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface RestApiService {

    public String basePath = "wp-json/wp/v2/";

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getLatestNews(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order);

    @GET(basePath+"media/{imageId}")
    Call<WPMediaDetailResponse> getMediaById(@Path(value="imageId", encoded=true) int imageId);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getMainNews(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order, @Query("categories_exclude") int categories_exclude);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getAdvertiser(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order, @Query("categories") int categories);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getPopularNews(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order, @Query("categories_exclude") int categories_exclude);

    @GET(basePath+"categories")
    Call<List<WPCategoriesResponse>> getCategories(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getNewsByCategory(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order, @Query("categories") long categories);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> searchNewsByCategory(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order, @Query("search") String search, @Query("categories") long categories);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getNewsVideo(@Query("page") int page, @Query("per_page") int per_page, @Query("order") String order,  @Query("categories") int categories);

    @GET(basePath+"posts")
    Call<List<WPNewsResponse>> getSearchNews(@Query("page") int page,  @Query("per_page") int per_page, @Query("order") String order, @Query("categories") int categories, @Query("search") String search, @Query("categories_exclude") int categories_exclude);

    @GET(basePath+"posts/{newsId}")
    Call<WPNewsResponse> getDetailNews(@Path(value="newsId", encoded=true) long newsId);

    @GET(basePath+"users/{authorId}")
    Call<WPAuthorResponse> getDetailAuthor(@Path(value="authorId", encoded=true) long authorId);

    @GET("api/public/v1/news/related/{keyword}")
    Call<NewsResponse> getRelatedNews(@Path(value="keyword", encoded=true) String keyword, @Query("page") int page, @Query("size") int size, @Query("sort") String sort, @Query("categories_exclude") int categories_exclude);

    @GET("api/auth/v1/users/{username}")
    Call<UsersResponse> getUsersByUsername(@Path(value="username", encoded=true) String username);

    @POST("api/public/v1/users")
    Call<CommonResponse> registerUser(@Body UsersReq users);

    @FormUrlEncoded
    @POST("api/oauth/token")
    Call<LoginUsersResponse> postLogin(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type);

    @PUT("api/public/v1/news/{id}")
    Call<CommonResponse> updateViews(@Path(value="id", encoded=true) long id);

    @GET("/api/public/v1/newsComment/getByNewsId/{newsId}")
    Call<NewsCommentResponse> getNewsComment(@Path(value="newsId", encoded=true) long id);

    @POST("api/auth/v1/newsComment")
    Call<CommonResponse> submitComment(@Body NewsCommentReq newsCommentReq);

    @GET("/api/public/v1/pushNotification")
    Call<NotificationResponse> getPushNotification();

    @GET("/api/public/v1/settingApplication")
    Call<ApplicationSettingResponse> getApplicationSetting();

    @GET("/api/public/v1/pushNotification/count")
    Call<CountNotificationResponse> getCountNotification();

}


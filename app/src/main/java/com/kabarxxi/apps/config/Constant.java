package com.kabarxxi.apps.config;

import com.kabarxxi.apps.response.WPCategoriesResponse;

import java.util.ArrayList;
import java.util.List;

public class Constant {

    public static final int ADVERTISER_CATEGORY = 397;
    public static final int TIPS_CATEGORY = 39;
    public static final int OPINION_CATEGORY = 40;
    public static final int VIDEO_CATEGORY = 25;
    public static List<WPCategoriesResponse> listWpCategories = new ArrayList<>();
    public static String utubeIframe = "<style>\n" +
            ".video-container { \n" +
            "position: relative; \n" +
            "padding-bottom: 56.25%; \n" +
            "padding-top: 35px; \n" +
            "height: 0; \n" +
            "overflow: hidden; \n" +
            "}\n" +
            ".video-container iframe { \n" +
            "position: absolute; \n" +
            "top:0; \n" +
            "left: 0; \n" +
            "width: 100%; \n" +
            "height: 100%; \n" +
            "}\n" +
            "</style>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div class=\"video-container\">\n" +
            "    <iframe src=\"youtubeUri\" allowfullscreen=\"\" frameborder=\"0\">\n" +
            "    </iframe>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";

    public static final int HIGH_PRIORITY_UPDATE = 5;
    public static final int MY_REQUEST_CODE = 9;


}

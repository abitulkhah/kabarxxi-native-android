package com.kabarxxi.apps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kabarxxi.apps.activities.DetailNewsActivity;
import com.kabarxxi.apps.fragments.CategoryFragment;
import com.kabarxxi.apps.fragments.HomeFragment;
import com.kabarxxi.apps.fragments.ProfileFragment;
import com.kabarxxi.apps.fragments.VideoFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private int counter = 0;
    public static final int NUMBER_OF_ADS = 5;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    Fragment fragmentHome = new HomeFragment();;
    Fragment categoryFragment = new CategoryFragment();;
    Fragment videoFragment = new VideoFragment();;
    Fragment profileFragment = new ProfileFragment();
    Fragment activeFragment = fragmentHome;
    FragmentManager fm = getSupportFragmentManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fm.beginTransaction().add(R.id.content, categoryFragment, "F_CATEGORY").hide(categoryFragment).commit();
        fm.beginTransaction().add(R.id.content, fragmentHome, "F_HOME").commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        registerTokenFCM();
        registerAdmob();

        Intent getIntent = getIntent();

        if(getIntent.getStringExtra("newsId")!=null){
            Intent intent = new Intent(this, DetailNewsActivity.class);

            intent.putExtra("newsId", getIntent.getStringExtra("newsId"));
            intent.putExtra("keyword", getIntent.getStringExtra("keyword"));
            intent.putExtra("newsTitle", getIntent.getStringExtra("newsTitle"));
            intent.putExtra("newsTime", getIntent.getStringExtra("newsTime"));
            intent.putExtra("description", getIntent.getStringExtra("description"));
            intent.putExtra("imageUri", getIntent.getStringExtra("imageUri"));
            intent.putExtra("categoryName", getIntent.getStringExtra("categoryName"));
            intent.putExtra("releaseBy", getIntent.getStringExtra("releaseBy"));

            startActivity(intent);
        }

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportActionBar().setTitle(null);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    getSupportActionBar().setIcon(R.drawable.logo_kabar);
                    fm.beginTransaction().hide(activeFragment).show(fragmentHome).commit();
                    activeFragment = fragmentHome;
                    return true;
                case R.id.navigation_category:
                    getSupportActionBar().setTitle("KATEGORI");
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    fm.beginTransaction().hide(activeFragment).show(categoryFragment).commit();
                    activeFragment = categoryFragment;
                    return true;
                case R.id.navigation_video:
                    getSupportActionBar().setTitle("VIDEO");
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    if(!isExistFragment("F_VIDEO")){
                        fm.beginTransaction().add(R.id.content, videoFragment, "F_VIDEO").hide(videoFragment).commit();
                    }
                    fm.beginTransaction().hide(activeFragment).show(videoFragment).commit();
                    activeFragment = videoFragment;
                    return true;
                case R.id.navigation_profile:
                    getSupportActionBar().setTitle("PROFIL");
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    if(!isExistFragment("F_PROFILE")){
                        fm.beginTransaction().add(R.id.content, profileFragment, "F_PROFILE").hide(profileFragment).commit();
                    }
                    fm.beginTransaction().hide(activeFragment).show(profileFragment).commit();
                    activeFragment = profileFragment;
                    return true;
            }
            return false;
        }
    };

    public void registerTokenFCM(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        Log.d(TAG, token);
                    }
                });

        FirebaseMessaging.getInstance().subscribeToTopic("allnews")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        Log.d(TAG, "successs subscribe topic");
                    }
                });

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

    }

    public void registerAdmob(){
        MobileAds.initialize(this,
                getString(R.string.admob_app_id));
    }

    public boolean isExistFragment(String fragmentTag){

        return getSupportFragmentManager().findFragmentByTag(fragmentTag) != null ? true : false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        counter = 0;
    }

    @Override
    public void onBackPressed() {
        counter++;
        if(counter == 1) {
            Toast.makeText(MainActivity.this,"Tekan sekali lagi untuk keluar aplikasi", Toast.LENGTH_SHORT).show();
        } else {
            finish();
        }
    }
}

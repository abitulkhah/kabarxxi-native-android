package com.kabarxxi.apps.models;

import android.graphics.drawable.Drawable;

public class OthersNews {

    private String newsTitle;
    private String newsTime;
    private Drawable newsImage;

    public OthersNews(String newsTitle, String newsTime, Drawable newsImage) {
        this.newsTitle = newsTitle;
        this.newsTime = newsTime;
        this.newsImage = newsImage;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }

    public Drawable getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(Drawable newsImage) {
        this.newsImage = newsImage;
    }
}

package com.kabarxxi.apps.models;

import java.util.Date;

public class NewsVideo {

    private Long id;

    private String title;

    private String base64Video;

    private String mimeType;

    private String description;

    private Date createdDate;

    private Date updateDate;

    private String base64Thumbnail;

    private String duration;

    public NewsVideo(Long id, String title, String base64Video, String mimeType, String description, Date createdDate, Date updateDate, String base64Thumbnail, String duration) {
        this.id = id;
        this.title = title;
        this.base64Video = base64Video;
        this.mimeType = mimeType;
        this.description = description;
        this.createdDate = createdDate;
        this.updateDate = updateDate;
        this.base64Thumbnail = base64Thumbnail;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBase64Video() {
        return base64Video;
    }

    public void setBase64Video(String base64Video) {
        this.base64Video = base64Video;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getBase64Thumbnail() {
        return base64Thumbnail;
    }

    public void setBase64Thumbnail(String base64Thumbnail) {
        this.base64Thumbnail = base64Thumbnail;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}


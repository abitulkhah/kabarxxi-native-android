package com.kabarxxi.apps.models;

public class MostCommentedNews {

    private Integer countCommented;
    private String newsTitle;
    private String newsTime;

    public MostCommentedNews(Integer countCommented, String newsTitle, String newsTime) {
        this.countCommented = countCommented;
        this.newsTitle = newsTitle;
        this.newsTime = newsTime;
    }

    public Integer getCountCommented() {
        return countCommented;
    }

    public void setCountCommented(Integer countCommented) {
        this.countCommented = countCommented;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(String newsTime) {
        this.newsTime = newsTime;
    }
}

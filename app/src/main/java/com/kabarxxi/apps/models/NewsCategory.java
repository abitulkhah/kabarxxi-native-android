package com.kabarxxi.apps.models;

public class NewsCategory {
    private Long id;

    private String categoryName;

    private Long parentCategory;

    private String description;

    private String base64Image;

    private String mimeType;

    public NewsCategory(Long id, String categoryName, Long parentCategory, String description, String base64Image, String mimeType) {
        this.id = id;
        this.categoryName = categoryName;
        this.parentCategory = parentCategory;
        this.description = description;
        this.base64Image = base64Image;
        this.mimeType = mimeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Long parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}

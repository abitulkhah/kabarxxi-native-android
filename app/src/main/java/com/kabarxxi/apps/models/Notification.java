package com.kabarxxi.apps.models;

public class Notification {

    private Long id;
    private News news;
    private String title;
    private String description;

    public Notification(Long id, News news, String title, String description) {
        this.id = id;
        this.news = news;
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.kabarxxi.apps.models;

public class WPMediaDetail {

    private WPMediaSize sizes;

    public WPMediaDetail(WPMediaSize sizes) {
        this.sizes = sizes;
    }

    public WPMediaSize getSizes() {
        return sizes;
    }

    public void setSizes(WPMediaSize sizes) {
        this.sizes = sizes;
    }
}

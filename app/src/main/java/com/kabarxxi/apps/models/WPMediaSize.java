package com.kabarxxi.apps.models;

public class WPMediaSize {

    private WPMediaContent medium;
    private WPMediaContent thumbnail;
    private WPMediaContent td_265x320;

    public WPMediaSize(WPMediaContent medium, WPMediaContent thumbnail, WPMediaContent td_265x320) {
        this.medium = medium;
        this.thumbnail = thumbnail;
        this.td_265x320 = td_265x320;
    }

    public WPMediaContent getMedium() {
        return medium;
    }

    public void setMedium(WPMediaContent medium) {
        this.medium = medium;
    }

    public WPMediaContent getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(WPMediaContent thumbnail) {
        this.thumbnail = thumbnail;
    }

    public WPMediaContent getTd_265x320() {
        return td_265x320;
    }

    public void setTd_265x320(WPMediaContent td_265x320) {
        this.td_265x320 = td_265x320;
    }
}

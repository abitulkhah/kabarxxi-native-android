package com.kabarxxi.apps.models;

public class WpNewsTitle {

    private String rendered;

    public WpNewsTitle(String rendered) {
        this.rendered = rendered;
    }

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }

}

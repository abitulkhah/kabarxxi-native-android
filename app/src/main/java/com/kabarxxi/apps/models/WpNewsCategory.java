package com.kabarxxi.apps.models;

public class WpNewsCategory {

    private Integer[] categories;

    public WpNewsCategory(Integer[] categories) {
        this.categories = categories;
    }

    public Integer[] getCategories() {
        return categories;
    }

    public void setCategories(Integer[] categories) {
        this.categories = categories;
    }
}

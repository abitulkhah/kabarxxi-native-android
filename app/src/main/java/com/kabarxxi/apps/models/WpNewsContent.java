package com.kabarxxi.apps.models;

public class WpNewsContent {

    private String rendered;

    public WpNewsContent(String rendered) {
        this.rendered = rendered;
    }

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }
}

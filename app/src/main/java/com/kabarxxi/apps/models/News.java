package com.kabarxxi.apps.models;

import java.util.Date;

public class News {
    private Long id;

    private String title;

    private Category category;

    private Date releaseDate;

    private String imageName;

    private String base64Image;

    private String mimeType;

    private Boolean active;

    private Boolean main;

    private Boolean popular;

    private String keyword;

    private String description;

    private Long views;

    private Date createdDate;

    private Date updateDate;

    private String createdBy;

    private String updateBy;

    private String releaseBy;

    public News(Long id, String title, Category category, Date releaseDate, String imageName, String base64Image, String mimeType, Boolean active, Boolean main, Boolean popular, String keyword, String description, Long views, Date createdDate, Date updateDate, String createdBy, String updateBy, String releaseBy) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.releaseDate = releaseDate;
        this.imageName = imageName;
        this.base64Image = base64Image;
        this.mimeType = mimeType;
        this.active = active;
        this.main = main;
        this.popular = popular;
        this.keyword = keyword;
        this.description = description;
        this.views = views;
        this.createdDate = createdDate;
        this.updateDate = updateDate;
        this.createdBy = createdBy;
        this.updateBy = updateBy;
        this.releaseBy = releaseBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getMain() {
        return main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public Boolean getPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getReleaseBy() {
        return releaseBy;
    }

    public void setReleaseBy(String releaseBy) {
        this.releaseBy = releaseBy;
    }
}

package com.kabarxxi.apps.models;

import java.util.Date;

public class Category {

    private Long id;
    private Long parentCategory;
    private String categoryName;
    private String description;
    private String base64Image;
    private String mimeType;
    private Date createdDate;
    private Date updatedDate;

    public Category(Long id, Long parentCategory, String categoryName, String description, String base64Image, String mimeType, Date createdDate, Date updatedDate) {
        this.id = id;
        this.parentCategory = parentCategory;
        this.categoryName = categoryName;
        this.description = description;
        this.base64Image = base64Image;
        this.mimeType = mimeType;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Long parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}

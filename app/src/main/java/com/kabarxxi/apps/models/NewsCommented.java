package com.kabarxxi.apps.models;

public class NewsCommented {

    private Long id;
    private News news;
    private Users users;
    private String description;

    public NewsCommented(Long id, News news, Users users, String description) {
        this.id = id;
        this.news = news;
        this.users = users;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

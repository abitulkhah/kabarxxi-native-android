package com.kabarxxi.apps.models;

import java.util.Date;

public class ApplicationSetting {
    private Long id;

    private String appName;

    private String version;

    private Integer androidVersionCode;

    private Integer iosVersionCode;

    private String description;

    private String copyright;

    private String imageName;

    private String base64Image;

    private String mimeType;

    private Date createdDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAndroidVersionCode() {
        return androidVersionCode;
    }

    public void setAndroidVersionCode(Integer androidVersionCode) {
        this.androidVersionCode = androidVersionCode;
    }

    public Integer getIosVersionCode() {
        return iosVersionCode;
    }

    public void setIosVersionCode(Integer iosVersionCode) {
        this.iosVersionCode = iosVersionCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}

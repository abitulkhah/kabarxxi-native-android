package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.models.NewsCommented;

import java.util.List;

public class NewsCommentedAdapter extends RecyclerView.Adapter<NewsCommentedAdapter.MyViewHolder> {

    private Context mContext;
    private List<NewsCommented> newsCommentedList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView commentUser, commentDescription;
        public ImageView commentProfile;

        public MyViewHolder(View view) {
            super(view);
            commentUser = (TextView) view.findViewById(R.id.commentUser);
            commentDescription = (TextView) view.findViewById(R.id.commentDescription);
            commentProfile = (ImageView) view.findViewById(R.id.commentProfile);
        }
    }


    public NewsCommentedAdapter(Context mContext, List<NewsCommented> newsCommentedList) {
        this.mContext = mContext;
        this.newsCommentedList = newsCommentedList;
    }

    @Override
    public NewsCommentedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_comment_card, parent, false);

        return new NewsCommentedAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsCommentedAdapter.MyViewHolder holder, int position) {
        NewsCommented newsCommented = newsCommentedList.get(position);
        holder.commentUser.setText(newsCommented.getUsers().getUsername());
        holder.commentDescription.setText(newsCommented.getDescription());
//        holder.commentProfile.setImageDrawable();
    }


    @Override
    public int getItemCount() {
        return newsCommentedList.size();
    }
}

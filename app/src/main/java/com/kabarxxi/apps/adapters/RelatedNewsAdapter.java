package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.DetailNewsActivity;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.utils.LocaleDateFormat;
import com.kabarxxi.apps.utils.PicassoRender;

import java.util.List;

public class RelatedNewsAdapter extends RecyclerView.Adapter<RelatedNewsAdapter.MyViewHolder> {

    private Context mContext;
    private List<WPNewsResponse> newsList;

    private static final int AD_TYPE = 0;

    // The unified native ad view type.
    private static final int CONTENT_TYPE = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView relatedNewsTitle, relatedNewsTime;

        public MyViewHolder(View view) {
            super(view);
            relatedNewsTitle = (TextView) view.findViewById(R.id.relatedNewsTitle);
            relatedNewsTime = (TextView) view.findViewById(R.id.relatedNewsTime);
        }
    }


    public RelatedNewsAdapter(Context mContext, List<WPNewsResponse> newsList) {
        this.mContext = mContext;
        this.newsList = newsList;
    }

    @Override
    public RelatedNewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        if (viewType == AD_TYPE) {

            view = new AdView(mContext);
            ((AdView) view).setAdSize(AdSize.SMART_BANNER);
            ((AdView) view).setAdUnitId("ca-app-pub-7106548430978346/1888045592");
            ((AdView) view).loadAd(new AdRequest.Builder().build());
//            ((AdView) view).loadAd(new AdRequest.Builder().addTestDevice("91C315C6B594A47AB3861549EFCA31F4").build());
        }else{

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_news_card, parent, false);

        }

        return new RelatedNewsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RelatedNewsAdapter.MyViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {
            case AD_TYPE:

                break;

            default:

                final MyViewHolder myViewHolder = (MyViewHolder) holder;
                final WPNewsResponse news = newsList.get(position);
                myViewHolder.relatedNewsTitle.setText(news.getTitle().getRendered());
                PicassoRender picassoRender = new PicassoRender();
                picassoRender.relatedAdapter(news.getFeatured_media(), myViewHolder, mContext, news);
                LocaleDateFormat localeDateFormat = new LocaleDateFormat();
//                final String dateNews = localeDateFormat.showLocaleDate(news.getReleaseDate(), "EEEE, dd MMMM yyyy HH:mm:ss");
                final String dateNews = localeDateFormat.getTimeAgo(news.getDate());
                myViewHolder.relatedNewsTime.setText(dateNews);

                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mContext, DetailNewsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("newsId", news.getId());
                        intent.putExtra("authorId", news.getAuthor());
                        intent.putExtra("newsTitle", news.getTitle().getRendered());
                        intent.putExtra("newsTime", dateNews);
                        intent.putExtra("content", news.getContent().getRendered());
                        intent.putExtra("imageUri", news.getMedia_details().getSizes().getMedium().getSource_url());

                        mContext.startActivity(intent);

                    }
                });

        }

    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position > 0 && position % 7 == 0)
            return AD_TYPE;
        return CONTENT_TYPE;
    }
}

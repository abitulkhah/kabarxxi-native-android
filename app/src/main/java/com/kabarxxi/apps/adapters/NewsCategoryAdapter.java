package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.BigNewsActivity;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.response.WPCategoriesResponse;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsCategoryAdapter extends RecyclerView.Adapter<NewsCategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<WPCategoriesResponse> newsCategoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsCategoryTitle;
        public ImageView newsCategoryImage;

        public MyViewHolder(View view) {
            super(view);
            newsCategoryTitle = (TextView) view.findViewById(R.id.newsCategoryTitle);
            newsCategoryImage = (ImageView) view.findViewById(R.id.newsCategoryImage);
        }
    }


    public NewsCategoryAdapter(Context mContext, List<WPCategoriesResponse> newsCategoryList) {
        this.mContext = mContext;
        this.newsCategoryList = newsCategoryList;
    }

    @Override
    public NewsCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_category_card, parent, false);

        return new NewsCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsCategoryAdapter.MyViewHolder holder, int position) {

        final WPCategoriesResponse newsCategory = newsCategoryList.get(position);
        holder.newsCategoryTitle.setText(newsCategory.getName());

        Picasso mPicasso = Picasso.with(mContext);
//        mPicasso.with(this.mContext)
//                .load(Config.IMAGE_URL+newsCategory.getBase64Image())
//                .into(holder.newsCategoryImage, new Callback() {
//                    @Override
//                    public void onSuccess() {
//
//                    }
//
//                    @Override
//                    public void onError() {
//                        //Try again online if cache failed
//                        Picasso.with(mContext)
//                                .load(Config.IMAGE_URL+newsCategory.getBase64Image())
//                                .error(R.drawable.no_image)
//                                .into(holder.newsCategoryImage, new Callback() {
//                                    @Override
//                                    public void onSuccess() {
//
//                                    }
//
//                                    @Override
//                                    public void onError() {
//                                        Log.v("Picasso","Could not fetch image");
//                                    }
//                                });
//                    }
//                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, BigNewsActivity.class);
                intent.putExtra("newsCategoryId", newsCategory.getId());
                intent.putExtra("newsCategoryName", newsCategory.getName());

                mContext.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {
        return newsCategoryList.size();
    }
}

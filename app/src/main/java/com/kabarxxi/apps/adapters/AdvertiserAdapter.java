package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.Advertiser;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.utils.PicassoRender;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdvertiserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

private Context mContext;
private List<Object> advertiserList;
private List<News> advertiserBookmarks;
private static final int AD_TYPE=0;

// The unified native ad view type.
private static final int CONTENT_TYPE=1;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public ImageView advertiserImage;

    public MyViewHolder(View view) {
        super(view);
        advertiserImage = (ImageView) view.findViewById(R.id.advertiserImage);
    }

}


    public AdvertiserAdapter(Context mContext, List<Object> advertiserList) {
        this.mContext = mContext;
        this.advertiserList = advertiserList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        if (viewType == AD_TYPE) {

            view = new AdView(mContext);
            ((AdView) view).setAdSize(AdSize.SMART_BANNER);
            ((AdView) view).setAdUnitId("ca-app-pub-7106548430978346/3999203610");
            ((AdView) view).loadAd(new AdRequest.Builder().build());
            ((AdView) view).setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    System.out.println("on onAdLoaded");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // Code to be executed when an ad request fails.
                    System.out.println("on onAdFailedToLoad");
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                    System.out.println("on onAdOpened");
                }

                @Override
                public void onAdClicked() {
                    // Code to be executed when the user clicks on an ad.
                    System.out.println("on onAdClicked");
                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                    System.out.println("on onAdLeftApplication");
                }

                @Override
                public void onAdClosed() {
                    // Code to be executed when the user is about to return
                    // to the app after tapping on an ad.
                    System.out.println("on onAdClosed");
                }
            });
//            ((AdView) view).loadAd(new AdRequest.Builder().addTestDevice("91C315C6B594A47AB3861549EFCA31F4").build());

        } else {

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.advertiser_card, parent, false);

        }

        return new AdvertiserAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {
            case AD_TYPE:


                break;

            default:

                final AdvertiserAdapter.MyViewHolder myViewHolder = (AdvertiserAdapter.MyViewHolder) holder;
                final WPNewsResponse advertiser = (WPNewsResponse) advertiserList.get(position);

                Picasso mPicasso = Picasso.with(mContext);
                PicassoRender picassoRender = new PicassoRender();
                picassoRender.advertiserAdapter(advertiser.getFeatured_media(), myViewHolder , mContext);
        }


    }


    @Override
    public int getItemCount() {
        return advertiserList.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position > 0 && position % 4 == 0)
            return AD_TYPE;
        return CONTENT_TYPE;
    }
}
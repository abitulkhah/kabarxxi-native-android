package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.DetailNewsActivity;
import com.kabarxxi.apps.models.Notification;
import com.kabarxxi.apps.utils.LocaleDateFormat;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private Context mContext;
    private List<Notification> notificationList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsTitle, newsTime;

        public MyViewHolder(View view) {
            super(view);
            newsTitle = (TextView) view.findViewById(R.id.notificationTitle);
            newsTime = (TextView) view.findViewById(R.id.notificationTime);
        }
    }


    public NotificationAdapter(Context mContext, List<Notification> notificationList) {
        this.mContext = mContext;
        this.notificationList = notificationList;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_card, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.MyViewHolder holder, int position) {
        final Notification notification = notificationList.get(position);
        LocaleDateFormat localeDateFormat = new LocaleDateFormat();
//        final String dateNews = localeDateFormat.showLocaleDate(notification.getNews().getReleaseDate(), "EEEE, dd MMMM yyyy HH:mm:ss");
        final String dateNews = localeDateFormat.getTimeAgo(notification.getNews().getReleaseDate());
        holder.newsTitle.setText(notification.getNews().getTitle());
        holder.newsTime.setText(dateNews);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DetailNewsActivity.class);
                intent.putExtra("newsId", notification.getNews().getId());
                intent.putExtra("keyword", notification.getNews().getKeyword());
                intent.putExtra("newsTitle", notification.getNews().getTitle());
                intent.putExtra("newsTime", dateNews);
                intent.putExtra("description", notification.getNews().getDescription());
                intent.putExtra("imageUri", notification.getNews().getBase64Image());
                intent.putExtra("categoryName", notification.getNews().getCategory().getCategoryName());
                intent.putExtra("releaseBy", notification.getNews().getReleaseBy());

                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return notificationList.size();
    }
}

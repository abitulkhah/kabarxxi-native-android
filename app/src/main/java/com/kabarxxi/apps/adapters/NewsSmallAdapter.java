package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.BookmarkActivity;
import com.kabarxxi.apps.activities.DetailNewsActivity;
import com.kabarxxi.apps.models.News;
import com.kabarxxi.apps.response.WPMediaDetailResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.kabarxxi.apps.utils.LocaleDateFormat;
import com.kabarxxi.apps.utils.PicassoRender;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsSmallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Object> newsList;
    private List<WPNewsResponse> newsBookmarks;

    private static final int AD_TYPE = 0;

    // The unified native ad view type.
    private static final int CONTENT_TYPE = 1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsTitle, newsTime, newsViewsCount;
        public ImageView newsImage;
        public ImageButton newsBookmarkSmall;
        public RelativeLayout smallListRL;

        public MyViewHolder(View view) {
            super(view);
            newsTitle = (TextView) view.findViewById(R.id.newsTitleSmall);
            newsTime = (TextView) view.findViewById(R.id.newsTimeSmall);
            newsImage = (ImageView) view.findViewById(R.id.newsImageSmall);
//            newsViewsCount = (TextView) view.findViewById(R.id.newsViewsCount);
            newsBookmarkSmall = (ImageButton) view.findViewById(R.id.newsBookmarkSmall);
            smallListRL = (RelativeLayout) view.findViewById(R.id.smallListRL);
        }
    }


    public NewsSmallAdapter(Context mContext, List<Object> newsList) {
        this.mContext = mContext;
        this.newsList = newsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        if (viewType == AD_TYPE) {

            view = new AdView(mContext);
            ((AdView) view).setAdSize(AdSize.SMART_BANNER);
            ((AdView) view).setAdUnitId("ca-app-pub-7106548430978346/1888045592");
            ((AdView) view).loadAd(new AdRequest.Builder().build());

            ((AdView) view).setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    System.out.println("on onAdLoaded");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // Code to be executed when an ad request fails.
                    System.out.println("on onAdFailedToLoad");
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                    System.out.println("on onAdOpened");
                }

                @Override
                public void onAdClicked() {
                    // Code to be executed when the user clicks on an ad.
                    System.out.println("on onAdClicked");
                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                    System.out.println("on onAdLeftApplication");
                }

                @Override
                public void onAdClosed() {
                    // Code to be executed when the user is about to return
                    // to the app after tapping on an ad.
                    System.out.println("on onAdClosed");
                }
            });
        }else{

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_small_card, parent, false);

        }
        return new NewsSmallAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {

            case AD_TYPE:

                break;

            default:

                final MyViewHolder myViewHolder = (MyViewHolder) holder;
                final WPNewsResponse news = (WPNewsResponse) newsList.get(position);

                PicassoRender picassoRender = new PicassoRender();
                picassoRender.smallAdapter(news.getFeatured_media(), myViewHolder, mContext, news );
//                myViewHolder.newsTitle.setText(news.getTitle());
                myViewHolder.newsTitle.setText(news.getTitle().getRendered());

                LocaleDateFormat localeDateFormat = new LocaleDateFormat();
//                final String dateNews = localeDateFormat.showLocaleDate(news.getReleaseDate(), "EEEE, dd MMMM yyyy HH:mm:ss");
                final String dateNews = localeDateFormat.getTimeAgo(news.getDate());

                myViewHolder.newsTime.setText(dateNews);
//                myViewHolder.newsViewsCount.setText(news.getViews()+" dilihat");
//                getDetailImage(news.getFeatured_media(), myViewHolder);

                myViewHolder.smallListRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mContext, DetailNewsActivity.class);
                        intent.putExtra("newsId", news.getId());
                        intent.putExtra("authorId", news.getAuthor());
                        intent.putExtra("newsTitle", news.getTitle().getRendered());
                        intent.putExtra("newsTime", dateNews);
                        intent.putExtra("content", news.getContent().getRendered());
                        intent.putExtra("imageUri", news.getMedia_details().getSizes().getMedium().getSource_url());
                        mContext.startActivity(intent);

                    }
                });

                myViewHolder.newsBookmarkSmall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = mContext.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
                        String bookmarkData = sharedPreferences.getString("bookmarkList", "");
                        boolean existData = false;

                        Gson gson = new Gson();

                        Type type = new TypeToken<List<WPNewsResponse>>() {}.getType();

                        if(bookmarkData.equalsIgnoreCase("") || bookmarkData.equalsIgnoreCase(null)){
                            newsBookmarks = new ArrayList<WPNewsResponse>();
                        }else {
                            newsBookmarks = gson.fromJson(bookmarkData, type);
                            for(WPNewsResponse newsCheck: newsBookmarks){
                                if(newsCheck.getId() == news.getId()){
                                    existData = true;
                                }
                            }
                        }


                        if(!existData){
                            newsBookmarks.add(news);
                            String json = gson.toJson(newsBookmarks);

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("bookmarkList", json);
                            editor.commit();
                        }

                        Toast.makeText(mContext, "Menambahkan Ke Bookmark", Toast.LENGTH_SHORT).show();


                    }
                });
        }


    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }


    @Override
    public int getItemViewType(int position) {

        if (position > 0 && position % 4 == 0)
            return AD_TYPE;
        return CONTENT_TYPE;
    }

}

package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.models.SearchNews;

import java.util.List;

public class SearchNewsAdapter extends RecyclerView.Adapter<SearchNewsAdapter.MyViewHolder> {

private Context mContext;
private List<SearchNews> mostPopularNewsList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView searchNewsTitle, searchNewsTime;
    public ImageView searchNewsImage;

    public MyViewHolder(View view) {
        super(view);
        searchNewsTitle = (TextView) view.findViewById(R.id.searchNewsTitle);
        searchNewsTime = (TextView) view.findViewById(R.id.searchNewsTime);
        searchNewsImage = (ImageView) view.findViewById(R.id.searchNewsImage);
    }
}


    public SearchNewsAdapter(Context mContext, List<SearchNews> mostPopularNewsList) {
        this.mContext = mContext;
        this.mostPopularNewsList = mostPopularNewsList;
    }

    @Override
    public SearchNewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_news_card, parent, false);

        return new SearchNewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SearchNewsAdapter.MyViewHolder holder, int position) {
        SearchNews mostPopularNews = mostPopularNewsList.get(position);
        holder.searchNewsTitle.setText(mostPopularNews.getNewsTitle());
        holder.searchNewsTime.setText(mostPopularNews.getNewsTime());
        holder.searchNewsImage.setImageDrawable(mostPopularNews.getNewsImage());
    }


    @Override
    public int getItemCount() {
        return mostPopularNewsList.size();
    }
}


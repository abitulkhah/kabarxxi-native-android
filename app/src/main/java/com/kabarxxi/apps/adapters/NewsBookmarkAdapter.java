package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.models.NewsBookmark;

import java.util.List;

public class NewsBookmarkAdapter extends RecyclerView.Adapter<NewsBookmarkAdapter.MyViewHolder> {

    private Context mContext;
    private List<NewsBookmark> newsBookmarkList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bookmarkTitle, bookmarkTime;
        public ImageView bookmarkImage;

        public MyViewHolder(View view) {
            super(view);
            bookmarkTitle = (TextView) view.findViewById(R.id.bookmarkTitle);
            bookmarkTime = (TextView) view.findViewById(R.id.bookmarkTime);
            bookmarkImage = (ImageView) view.findViewById(R.id.bookmarkImage);
        }
    }


    public NewsBookmarkAdapter(Context mContext, List<NewsBookmark> newsBookmarkList) {
        this.mContext = mContext;
        this.newsBookmarkList = newsBookmarkList;
    }

    @Override
    public NewsBookmarkAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_bookmark_card, parent, false);

        return new NewsBookmarkAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsBookmarkAdapter.MyViewHolder holder, int position) {
        NewsBookmark newsBookmark = newsBookmarkList.get(position);
        holder.bookmarkTitle.setText(newsBookmark.getNewsTitle());
        holder.bookmarkTime.setText(newsBookmark.getNewsTime());
        holder.bookmarkImage.setImageDrawable(newsBookmark.getNewsImage());
    }


    @Override
    public int getItemCount() {
        return newsBookmarkList.size();
    }
}

package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.models.OthersNews;

import java.util.List;

public class OthersNewsAdapter extends RecyclerView.Adapter<OthersNewsAdapter.MyViewHolder> {

    private Context mContext;
    private List<OthersNews> othersNewsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsTitle, newsTime;
        public ImageView newsImage;

        public MyViewHolder(View view) {
            super(view);
//            newsTitle = (TextView) view.findViewById(R.id.othersNewsTitle);
//            newsTime = (TextView) view.findViewById(R.id.othersNewsTime);
//            newsImage = (ImageView) view.findViewById(R.id.othersNewsImage);
        }
    }


    public OthersNewsAdapter(Context mContext, List<OthersNews> othersNewsList) {
        this.mContext = mContext;
        this.othersNewsList = othersNewsList;
    }

    @Override
    public OthersNewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_small_card, parent, false);

        return new OthersNewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OthersNewsAdapter.MyViewHolder holder, int position) {
        OthersNews othersNews = othersNewsList.get(position);
        holder.newsTitle.setText(othersNews.getNewsTitle());
        holder.newsTime.setText(othersNews.getNewsTime());
        holder.newsImage.setImageDrawable(othersNews.getNewsImage());
    }


    @Override
    public int getItemCount() {
        return othersNewsList.size();
    }
}

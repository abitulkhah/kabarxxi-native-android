package com.kabarxxi.apps.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabarxxi.apps.R;
import com.kabarxxi.apps.activities.DetailVideoActivity;
import com.kabarxxi.apps.config.Config;
import com.kabarxxi.apps.models.NewsVideo;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.utils.LocaleDateFormat;
import com.kabarxxi.apps.utils.PicassoRender;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsVideoAdapter extends RecyclerView.Adapter<NewsVideoAdapter.MyViewHolder> {

    private Context mContext;
    private List<WPNewsResponse> newsVideoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView newsVideoTitle, newsVideoTime;
        public ImageView newsVideoThumbnail;
        public ImageButton imagePlayButton;

        public MyViewHolder(View view) {
            super(view);
            newsVideoTitle = (TextView) view.findViewById(R.id.newsVideoTitle);
            newsVideoTime = (TextView) view.findViewById(R.id.newsVideoTime);
            newsVideoThumbnail = (ImageView) view.findViewById(R.id.newsVideoThumbnail);
            imagePlayButton = (ImageButton) view.findViewById(R.id.imagePlayButton);

        }
    }


    public NewsVideoAdapter(Context mContext, List<WPNewsResponse> newsVideoList) {
        this.mContext = mContext;
        this.newsVideoList = newsVideoList;
    }

    @Override
    public NewsVideoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_video_card, parent, false);

        return new NewsVideoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsVideoAdapter.MyViewHolder holder, int position) {

        final WPNewsResponse newsVideo = newsVideoList.get(position);

        PicassoRender picassoRender = new PicassoRender();
        picassoRender.videoAdapter(newsVideo.getFeatured_media(), holder, mContext, newsVideo);

        holder.newsVideoTitle.setText(newsVideo.getTitle().getRendered());
        LocaleDateFormat localeDateFormat = new LocaleDateFormat();
        final String dateNews = localeDateFormat.getTimeAgo(newsVideo.getDate());
        holder.newsVideoTime.setText(dateNews);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DetailVideoActivity.class);
                intent.putExtra("newsId", newsVideo.getId());
                intent.putExtra("authorId", newsVideo.getAuthor());
                intent.putExtra("newsTitle", newsVideo.getTitle().getRendered());
                intent.putExtra("newsTime", dateNews);
                intent.putExtra("content", newsVideo.getContent().getRendered());
                intent.putExtra("imageUri", newsVideo.getMedia_details().getSizes().getMedium().getSource_url());
                mContext.startActivity(intent);
            }
        });

        holder.imagePlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, DetailVideoActivity.class);
                intent.putExtra("newsId", newsVideo.getId());
                intent.putExtra("authorId", newsVideo.getAuthor());
                intent.putExtra("newsTitle", newsVideo.getTitle().getRendered());
                intent.putExtra("newsTime", dateNews);
                intent.putExtra("content", newsVideo.getContent().getRendered());
                intent.putExtra("imageUri", newsVideo.getMedia_details().getSizes().getMedium().getSource_url());
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return newsVideoList.size();
    }
}

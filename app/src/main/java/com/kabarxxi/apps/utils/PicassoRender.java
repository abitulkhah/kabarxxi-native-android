package com.kabarxxi.apps.utils;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.kabarxxi.apps.activities.BookmarkActivity;
import com.kabarxxi.apps.adapters.AdvertiserAdapter;
import com.kabarxxi.apps.adapters.NewsBigAdapter;
import com.kabarxxi.apps.adapters.NewsSmallAdapter;
import com.kabarxxi.apps.adapters.NewsVideoAdapter;
import com.kabarxxi.apps.adapters.RelatedNewsAdapter;
import com.kabarxxi.apps.response.WPMediaDetailResponse;
import com.kabarxxi.apps.response.WPNewsResponse;
import com.kabarxxi.apps.rest.RestApiService;
import com.kabarxxi.apps.rest.RetrofitClientInstance;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PicassoRender {

    public void smallAdapter(int mediaId, final NewsSmallAdapter.MyViewHolder myViewHolder, final Context mContext, final WPNewsResponse wpNewsResponse){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(mediaId);

        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    Log.i("image response", "onResponse: "+response);

                    if(mContext instanceof BookmarkActivity){
                        myViewHolder.newsBookmarkSmall.setVisibility(View.GONE);
                    }
                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    Log.i("wpImageDetail ==== ", "wpImageDetailResponse: "+ wpMediaDetailResponse);
                    wpNewsResponse.setMedia_details(wpMediaDetailResponse.getMedia_details());

                    Picasso mPicasso = Picasso.with(mContext);
                    mPicasso.with(mContext)
                            .load(wpMediaDetailResponse.getMedia_details().getSizes().getThumbnail().getSource_url())
                            .fit()
                            .into(myViewHolder.newsImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {
                System.out.println("errror image "+call);
                System.out.println("errror image t "+t);
            }
        });

    }

    public void bigAdapter(int mediaId, final NewsBigAdapter.MyViewHolder myViewHolder, final Context mContext, final WPNewsResponse wpNewsResponse){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(mediaId);

        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    Log.i("image response", "onResponse: "+response);

                    if(mContext instanceof BookmarkActivity){
                        myViewHolder.newsBookmarkBig.setVisibility(View.GONE);
                    }
                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    Log.i("wpImageDetail ==== ", "wpImageDetailResponse: "+ wpMediaDetailResponse);
                    wpNewsResponse.setMedia_details(wpMediaDetailResponse.getMedia_details());

                    Picasso mPicasso = Picasso.with(mContext);
                    mPicasso.with(mContext)
                            .load(wpMediaDetailResponse.getMedia_details().getSizes().getMedium().getSource_url())
                            .fit()
                            .into(myViewHolder.newsImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {
            }
        });

    }

    public void advertiserAdapter(int mediaId, final AdvertiserAdapter.MyViewHolder myViewHolder, final Context mContext){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(mediaId);

        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    Log.i("image response", "onResponse: "+response);

                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    Log.i("wpImageDetail ==== ", "wpImageDetailResponse: "+ wpMediaDetailResponse);

                    Picasso mPicasso = Picasso.with(mContext);
                    mPicasso.with(mContext)
                            .load(wpMediaDetailResponse.getMedia_details().getSizes().getTd_265x320().getSource_url())
                            .fit()
                            .into(myViewHolder.advertiserImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {
            }
        });

    }

    public void videoAdapter(int mediaId, final NewsVideoAdapter.MyViewHolder myViewHolder, final Context mContext, final WPNewsResponse wpNewsResponse){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(mediaId);

        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    Log.i("image response", "onResponse: "+response);

                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    Log.i("wpImageDetail ==== ", "wpImageDetailResponse: "+ wpMediaDetailResponse);
                    wpNewsResponse.setMedia_details(wpMediaDetailResponse.getMedia_details());

                    Picasso mPicasso = Picasso.with(mContext);
                    mPicasso.with(mContext)
                            .load(wpMediaDetailResponse.getMedia_details().getSizes().getThumbnail().getSource_url())
                            .fit()
                            .into(myViewHolder.newsVideoThumbnail, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {
            }
        });

    }

    public void relatedAdapter(int mediaId, final RelatedNewsAdapter.MyViewHolder myViewHolder, final Context mContext, final WPNewsResponse wpNewsResponse){

        RestApiService service = RetrofitClientInstance.getRetrofitInstance().create(RestApiService.class);
        Call<WPMediaDetailResponse> call = service.getMediaById(mediaId);

        call.enqueue(new Callback<WPMediaDetailResponse>() {
            @Override
            public void onResponse(Call<WPMediaDetailResponse> call, Response<WPMediaDetailResponse> response) {

                if(response.isSuccessful()){

                    final WPMediaDetailResponse wpMediaDetailResponse = (WPMediaDetailResponse) response.body();
                    wpNewsResponse.setMedia_details(wpMediaDetailResponse.getMedia_details());

                }

            }

            @Override
            public void onFailure(Call<WPMediaDetailResponse> call, Throwable t) {

            }
        });

    }


}

package com.kabarxxi.apps.request;

public class NewsCommentReq {

    private Long userId;
    private Long newsId;
    private String description;

    public NewsCommentReq(Long userId, Long newsId, String description) {
        this.userId = userId;
        this.newsId = newsId;
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
